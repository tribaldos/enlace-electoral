import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Hashtag, Mention } from 'react-twitter-widgets';

class TweetButton extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    to: PropTypes.string,
    hashtag: PropTypes.string,
    size: PropTypes.oneOf(['small', 'large']),
  }

  render() {
    const { text, to, hashtag, size } = this.props;
    let options = {
      text: text,
      screen_name: to,
      hashtags: hashtag,
      via: 'EnlaceElectoral',
      size: size || 'small',
    };

    return hashtag
           ? <Hashtag hashtag={ hashtag } options={ options } />
           : <Mention username={ to } options={ options } />;
  }
}

export default TweetButton;