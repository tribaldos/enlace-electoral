import React, { Component } from 'react';
import Layout from './Layout';
import FigurasGrid from './FigurasGrid';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout>
          <FigurasGrid />
        </Layout>
      </div>
    );
  }
}

export default App;