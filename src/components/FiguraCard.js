import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TweetButton from './TweetButton';

const styles = {
  card: {
    maxWidth: 264,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
};

function FiguraCard(props) {
  const { classes } = props;
  return (
    <div>
      <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image="https://randomuser.me/api/portraits/men/65.jpg"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="headline" component="h2">
            Lizard
          </Typography>
          <Typography component="p">
            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
            across all continents except Antarctica
          </Typography>
        </CardContent>
        <CardActions>
          <TweetButton
            text="Estamos esperando la publicación de su planilla"
            to="prueba"
            hashtag="PubliquenSuPlanilla"
            size="large" />
        </CardActions>
      </Card>
    </div>
  );
}

FiguraCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FiguraCard);