import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import FiguraCard from './FiguraCard';

class FigurasGrid extends Component {
  static propTypes = {};

  render() {
    return (
      <Grid container spacing={24} justify="center">
        <Grid item>
          <FiguraCard />
        </Grid>
        <Grid item>
          <FiguraCard />
        </Grid>
        <Grid item>
          <FiguraCard />
        </Grid>
        <Grid item>
          <FiguraCard />
        </Grid>
      </Grid>
    )
  }
}

export default FigurasGrid;